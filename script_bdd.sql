
/* Table niveau */
CREATE TABLE randonnee.niveau (
	id_niveau INT auto_increment NOT NULL,
	libelle_niveau varchar(100) NOT NULL,
	CONSTRAINT niveau_PK PRIMARY KEY (id_niveau)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

/* Table itineraire */
CREATE TABLE randonnee.itineraire (
    id_itineraire INT auto_increment NOT NULL,
	nom_itineraire varchar(100) NOT NULL,
    description_itineraire varchar(2000) NOT NULL,
	niveau_itineraire INT NOT NULL,
	CONSTRAINT itineraire_PK PRIMARY KEY (id_itineraire)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

/*Table etape*/
CREATE TABLE randonnee.etape (
	id_etape INT auto_increment NOT NULL,
	nom_etape VARCHAR(200) NOT NULL,
	qrcode_etape VARCHAR(255) NOT NULL,
	description_etape varchar(2000) NULL,
	likes_etape INT NULL,
	fk_id_itineraire INT NOT NULL,
	CONSTRAINT etape_PK PRIMARY KEY (id_etape)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

/* Table commentaire */
CREATE TABLE randonnee.commentaire (
	id_commentaire INT auto_increment NOT NULL,
	pseudo_commentaire varchar(200) NOT NULL,
	message_commentaire varchar(2000) NOT NULL,
	date_commentaire DATE NOT NULL,
	fk_id_etape INT NOT NULL,
	CONSTRAINT commentaire_PK PRIMARY KEY (id_commentaire)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

/* Table photo */
CREATE TABLE randonnee.photo (
	id_photo INT auto_increment NOT NULL,
	url_photo varchar(255) NOT NULL,
	fk_id_etape INT NOT NULL,
	CONSTRAINT photo_PK PRIMARY KEY (id_photo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

/* Table user */
CREATE TABLE randonnee.securite_user (
	id_user INT auto_increment NOT NULL,
	nom_user varchar(100) NOT NULL,
	pass_user varchar(100) NOT NULL,
	role_user varchar(100) NULL,
	CONSTRAINT securite_user_PK PRIMARY KEY (id_user)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

/* Ajout des constraintes sur les clés étrangères */
ALTER TABLE randonnee.itineraire ADD CONSTRAINT itineraire_FK FOREIGN KEY (niveau_itineraire) REFERENCES randonnee.niveau(id_niveau);
ALTER TABLE randonnee.etape ADD CONSTRAINT etape_FK FOREIGN KEY (id_itineraire) REFERENCES randonnee.itineraire(id_itineraire);
ALTER TABLE randonnee.commentaire ADD CONSTRAINT commentaire_FK FOREIGN KEY (fk_id_etape) REFERENCES randonnee.etape(id_etape);
ALTER TABLE randonnee.photo ADD CONSTRAINT photo_FK FOREIGN KEY (fk_id_etape) REFERENCES randonnee.etape(id_etape);


/* Ajout de donnée de test */
insert into niveau(libelle_niveau) values ('Débutant');
insert into niveau(libelle_niveau) values ('Intermédiaire');
insert into niveau(libelle_niveau) values ('Confirmé');

insert into itineraire(nom_itineraire, niveau_itineraire) values ('La route de la montagne', 1);
insert into itineraire(nom_itineraire, niveau_itineraire) values ('La route de la mer', 2);
insert into itineraire(nom_itineraire, niveau_itineraire) values ('La route des enfers', 3);

insert into etape(nom_etape, qrcode_etape, description_etape, likes_etape, fk_id_itineraire) values ('En bas de la montagne', 'https://google.com', 'une petite plaine face à la montagne', 2, 1);
insert into etape(nom_etape, qrcode_etape, description_etape, likes_etape, fk_id_itineraire) values ('Au milieu de la montagne', 'https://google.com', 'une cavité où il fait bon se mettre à l\'abri', 5, 1);
insert into etape(nom_etape, qrcode_etape, description_etape, likes_etape, fk_id_itineraire) values ('En haut de la montagne', 'https://google.com', 'un plateau eneigé mais qui sent votre victoire', 18, 1);

insert into commentaire(pseudo_commentaire, message_commentaire, date_commentaire, fk_id_etape) values ('Gégé du 33', 'Un régal au moins aussi grand que le dernier album de Jhonny', '2020-08-26', 1);
insert into commentaire(pseudo_commentaire, message_commentaire, date_commentaire, fk_id_etape) values ('Coquin du bassin', 'Ça manque de filles...', '2019-11-14', 2);
insert into commentaire(pseudo_commentaire, message_commentaire, date_commentaire, fk_id_etape) values ('Géraldine', 'un paysage somptueux', '2019-11-14', 3);

insert into photo(url_photo, fk_id_etape) values ('https://jeromeobiols.com/wordpress/wp-content/uploads/photo-montagne-vallee-blanche-chamonix-mont-blanc.jpg', 1);
insert into photo(url_photo, fk_id_etape) values ('https://www.alidifirenze.fr/wp-content/uploads/2016/09/montagne-italie-ali-di-firenze-19.jpg', 2);
insert into photo(url_photo, fk_id_etape) values ('https://cdn.radiofrance.fr/s3/cruiser-production/2020/07/bee8a944-97d3-4392-a0ef-030063659bce/1200x680_gettyimages-1125944394.jpg', 3);
insert into securite_user(nom_user, pass_user, role_user) values ('admin', 'admin', 'admin');

