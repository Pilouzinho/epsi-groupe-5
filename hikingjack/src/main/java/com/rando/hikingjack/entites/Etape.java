package com.rando.hikingjack.entites;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "etape")
public class Etape implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idEtape;
	
	@Column(name = "nom_etape")
	private String nomEtape;
	
	@Column(name = "qrcode_etape")
	private String qrCodeEtape;
	
	@Column(name = "description_etape")
	private String descriptionEtape;
	
	@Column(name = "likes_etape")
	private int likesEtape;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_itineraire")
	private Itineraire itineraireEtape;
	

	public Etape() {
		super();
	}

	public Etape(int idEtape, String nomEtape, String qrCodeEtape, String descriptionEtape, int likesEtape,
			Itineraire itineraireEtape) {
		super();
		this.idEtape = idEtape;
		this.nomEtape = nomEtape;
		this.qrCodeEtape = qrCodeEtape;
		this.descriptionEtape = descriptionEtape;
		this.likesEtape = likesEtape;
		this.itineraireEtape = itineraireEtape;
	}

	public Etape(String nomEtape, String qrCodeEtape, String descriptionEtape, int likesEtape,
			Itineraire itineraireEtape) {
		super();
		this.nomEtape = nomEtape;
		this.qrCodeEtape = qrCodeEtape;
		this.descriptionEtape = descriptionEtape;
		this.likesEtape = likesEtape;
		this.itineraireEtape = itineraireEtape;
	}

	public int getIdEtape() {
		return idEtape;
	}

	public void setIdEtape(int idEtape) {
		this.idEtape = idEtape;
	}

	public String getNomEtape() {
		return nomEtape;
	}

	public void setNomEtape(String nomEtape) {
		this.nomEtape = nomEtape;
	}

	public String getQrCodeEtape() {
		return qrCodeEtape;
	}

	public void setQrCodeEtape(String qrCodeEtape) {
		this.qrCodeEtape = qrCodeEtape;
	}

	public String getDescriptionEtape() {
		return descriptionEtape;
	}

	public void setDescriptionEtape(String descriptionEtape) {
		this.descriptionEtape = descriptionEtape;
	}

	public int getLikesEtape() {
		return likesEtape;
	}

	public void setLikesEtape(int likesEtape) {
		this.likesEtape = likesEtape;
	}

	public Itineraire getItineraireEtape() {
		return itineraireEtape;
	}

	public void setItineraireEtape(Itineraire itineraireEtape) {
		this.itineraireEtape = itineraireEtape;
	}

	@Override
	public String toString() {
		return "Etape [idEtape=" + idEtape + ", nomEtape=" + nomEtape + ", qrCodeEtape=" + qrCodeEtape
				+ ", descriptionEtape=" + descriptionEtape + ", likesEtape=" + likesEtape + ", itineraireEtape="
				+ itineraireEtape + "]";
	}
	
	
	
}
