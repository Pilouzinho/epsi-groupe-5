package com.rando.hikingjack.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.rando.hikingjack.entites.Commentaire;

public interface CommentaireRepository extends JpaRepository<Commentaire, Integer>  {

	@Query(value="select * from commentaire c where c.fk_id_etape = ?1", nativeQuery = true)
	List<Commentaire> findByEtapeId(int idEtape);
}
