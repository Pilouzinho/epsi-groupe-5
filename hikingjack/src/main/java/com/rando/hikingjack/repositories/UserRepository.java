package com.rando.hikingjack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rando.hikingjack.entites.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	
}
